<?php get_header(); ?>

    <div class="row">
        <div class="col-md-8">


            <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();?>


            <h1><?php the_title();?> </h1>
             <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail('medium_large');
            }
            ?>
                <div>

                    <p class="blog-post-meta">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <?php the_time( get_option( 'date_format' ) ); ?>/ <?php the_category(', '); ?>  <?php comments_number(); ?> </p>

                </div>
           

           <div  class="single-img-post"> 
             <?php the_content(); ?>
            </div> 

              <?php  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
            endwhile; endif;
            ?>

        </div> <!-- /.col -->

          <?php get_sidebar(); ?>

    </div> <!-- /.row -->

<?php get_footer(); ?>