<?php
add_theme_support( 'title-tag' );

// Support Featured Images
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
add_theme_support('custom-header');
add_theme_support('custom-background');
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'video', 'link', 'quote' ,'status' , 'audio' ,'chat') );

load_theme_textdomain('neutrino',get_template_directory_uri().'/languages');
register_nav_menu('psmainmenu',__('main menu','neutrino'));
// Set the default content width.
    $GLOBALS['content_width'] = 625;


function ps_neutrino_function() {


    // WordPress Titles



}



function ps_pagination() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /**	Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /**	Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /**	Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link("<") );

    /**	Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /**	Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /**	Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /**	Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link(">") );

    echo '</ul></div>' . "\n";

}

add_action( 'action', 'ps_neutrino_function');
add_action( 'action', 'ps_pagination');





//add awasomefonts 
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );
function enqueue_load_fa() {
 
    wp_enqueue_style( 'load-fa', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
 
}

// Add scripts and stylesheets
function startwordpress_scripts() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );
    wp_enqueue_style( 'bootstrap2', get_template_directory_uri() . '/css/font-awesome.min.css' );
    wp_enqueue_style( 'style2', get_template_directory_uri() . '/css/style2.css' );
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
}

add_action( 'wp_enqueue_scripts', 'startwordpress_scripts' );


// Add Google Fonts
//function startwordpress_google_fonts() {
//    wp_register_style('OpenSans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800');
//    wp_enqueue_style( 'OpenSans');
//}
//
//add_action('wp_print_styles', 'startwordpress_google_fonts');

// Changing excerpt more
function new_excerpt_more($more) {
    global $post;
    return '… <a href="'. get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


function my_widget()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar',
     'description'=> 'add right sidebar widget here',
     'id'=>'ps_right_sidebar'
    ));
}
add_action('widgets_init','my_widget');




function my_widget2()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar2',
     'description'=> 'add right sidebar widget here 2',
     'id'=>'ps_right_sidebar2'
    ));
}
add_action('widgets_init','my_widget2');

function my_widget3()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar3',
     'description'=> 'add right sidebar widget here 3',
     'id'=>'ps_right_sidebar3'
    ));
}
add_action('widgets_init','my_widget3');


function my_widget4()
{
   register_sidebar(array(

     'name'=> 'ps right sidebar4',
     'description'=> 'add right sidebar widget here 4',
     'id'=>'ps_right_sidebar4'
    ));
}
add_action('widgets_init','my_widget4');

?>
















<?php function sm_custom_meta() {
    add_meta_box( 'sm_meta', __( 'Featured Posts', 'sm-textdomain' ), 'sm_meta_callback', 'post' );
}
function sm_meta_callback( $post ) {
    $featured = get_post_meta( $post->ID );
    ?>
 
    <p>
    <div class="sm-row-content">
        <label for="meta-checkbox">
            <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $featured['meta-checkbox'] ) ) checked( $featured['meta-checkbox'][0], 'yes' ); ?> />
            <?php _e( 'Featured this post', 'sm-textdomain' )?>
        </label>
        
    </div>
</p>
 
    <?php
}
add_action( 'add_meta_boxes', 'sm_custom_meta' );
?>









<?php 
/**
 * Saves the custom meta input
 */
function sm_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'sm_nonce' ] ) && wp_verify_nonce( $_POST[ 'sm_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
 // Checks for input and saves
if( isset( $_POST[ 'meta-checkbox' ] ) ) {
    update_post_meta( $post_id, 'meta-checkbox', 'yes' );
} else {
    update_post_meta( $post_id, 'meta-checkbox', '' );
}
 
}
add_action( 'save_post', 'sm_meta_save' );
?>







<?php


// DIY Popular Posts @ https://digwp.com/2016/03/diy-popular-posts/
function shapeSpace_popular_posts($post_id) {
    $count_key = 'popular_posts';
    $count = get_post_meta($post_id, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($post_id, $count_key);
        add_post_meta($post_id, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_id, $count_key, $count);
    }
}
function shapeSpace_track_posts($post_id) {
    if (!is_single()) return;
    if (empty($post_id)) {
        global $post;
        $post_id = $post->ID;
    }
    shapeSpace_popular_posts($post_id);
}
add_action('wp_head', 'shapeSpace_track_posts');



// content sidebar: try

function content_sidebar()
{
    ?>
     
<!-- // Define our WP Query Parameters -->
<?php $the_query = new WP_Query( 'posts_per_page=5' ); ?>

<!-- // Start our WP Query -->
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

<!-- // Display the Post Title with Hyperlink -->
<h2 class="blog-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

    <p class="blog-post-meta">BY <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> ON <?php the_time( get_option( 'date_format' ) ); ?>/ <?php the_category(', '); ?>  <?php comments_number(); ?> </p>

<!-- // Repeat the process and reset once it hits the limit -->
<?php 
endwhile;
wp_reset_postdata();
?>

<?php
}
add_action( 'action', 'content_sidebar');



function my_new_contactmethods( $contactmethods ) {
    // Add Twitter
    $contactmethods['twitter'] = 'Twitter';
    //add Facebook
    $contactmethods['facebook'] = 'Facebook';

    //add Facebook
    $contactmethods['pinterest'] = 'Pinterest';

    //add Facebook
    $contactmethods['youtube'] = 'Youtube';

    $contactmethods['instagram'] = 'Instagram';


    return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);


 function m1_customize_register( $wp_customize ) {

   $wp_customize->add_setting( 'm1_logo' ); // Add setting for logo uploader



   // Add control for logo uploader (actual uploader)

   $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'm1_logo', array(

       'label'    => __( 'Upload 2nd Logo (replaces text)', 'm1' ),

       'section'  => 'title_tagline',

       'settings' => 'm1_logo',

   ) ) );

}

add_action( 'customize_register', 'm1_customize_register' );




