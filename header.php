<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title><?php echo get_bloginfo( 'name' ); ?></title>
    <link href="<?php echo get_bloginfo( 'template_directory' );?>/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head();?>

</head>

<body <?php body_class() ?> >




<!-- making custom menu -->
<div class="container-fluid">
    <div class="row">

        <div class="col-md-3 logo">
            <?php
            $custom_logo_id = get_theme_mod( 'custom_logo' );
            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
            ?>
            <a href="<?php echo get_home_url(); ?>">
                <img class="site-logo" src="<?php echo $image[0]; ?>" alt="not found">
            </a>
        </div>



        <div class="col-md-7">
            <div id="bar">
                <?php
                $args=array(
                    'theme_location'=>'primary'
                );
                ?>
                <?php wp_nav_menu($args); ?>
            </div>
        </div>


        <div class="col-md-2 social_icon_wrap">
            <?php
            $walt_id = 1; // Make sure you have the correct ID here
            $userdata = get_user_meta( $walt_id );
            ?>

            <a href="<?php  echo $userdata['facebook'][0]; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> </a>

            <a href="<?php  echo $userdata['twitter'][0]; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> </a>

            <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

            <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

            <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>


        </div>



    </div>
</div>
<!-- <?php echo get_home_url(); ?> -->
<div class="container">