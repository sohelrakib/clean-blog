</div> <!-- /.container -->

<footer class="blog-footer">
    <div class="container">
<div class="row footer-wrap">

    <div class="col-md-2 logo">

        <?php if ( get_theme_mod( 'm1_logo' ) ) : ?>

            <a href="<?php echo get_home_url(); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">



                <img src="<?php echo get_theme_mod( 'm1_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">



            </a>



        <?php else : ?>



            <hgroup>

                <h1 class="site-title"><a href="<?php echo get_home_url(); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

                <p class="site-description"><?php bloginfo( 'description' ); ?></p>

            </hgroup>



        <?php endif; ?>

    </div>



     <div class="col-md-7">
       <div id="footer-bar">
           <?php
           $args=array(
               'theme_location'=>'footer'
           );
           ?>
           <?php wp_nav_menu($args); ?>
      </div>
    </div>


    <div class="col-md-2 social_icon_wrap">
        <?php
        $walt_id = 1; // Make sure you have the correct ID here
        $userdata = get_user_meta( $walt_id );
        ?>

        <a href="<?php  echo $userdata['facebook'][0]; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i> </a>

        <a href="<?php  echo $userdata['twitter'][0]; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> </a>

        <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a>

        <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>

        <a href="<?php  echo $userdata['pinterest'][0]; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>


    </div>
</div>
</div>
  

    

   
</div>
    <p>
        <a href="#">Back to top</a>
    </p>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>