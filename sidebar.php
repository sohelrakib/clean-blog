<div class="col-md-4  blog-sidebar">

    <?php dynamic_sidebar('ps_right_sidebar');?>
    


      <div>
    <!--  https://paulund.co.uk/how-to-display-author-bio-with-wordpress -->
    <!-- https://wordpress.stackexchange.com/questions/207947/how-to-display-a-users-bio-not-the-author-or-logged-in-user -->
           <h3 class="about_us text-center">  About Us   </h3>

       <div class="author-bio text-center">
      
            <?php echo get_avatar( get_the_author_meta('email'), '90' ); ?>
            
            <div class="author-info">
                    <p class ="author-description">
                         <?php 

                         // the_author_meta('description');  /1st way 
                         // the_author_description();        /2nd way ,below 3rd and the best

                          $walt_id = 1; // Make sure you have the correct ID here
                          $userdata = get_user_meta( $walt_id );
                          echo $userdata['description'][0];


                           ?>
                    </p>
            </div>
      </div>
   </div>


  <?php dynamic_sidebar('ps_right_sidebar2');?>

    <!-- <div>
       <?php wp_list_categories(); ?>
    </div> -->





       <div>
             <h1> FEATURED POSTS:</h1>

          <!--    http://smallenvelop.com/how-to-create-featured-posts-in-wordpress/ -->

       <?php
  $args = array(
        'posts_per_page' => 5,
        'meta_key' => 'meta-checkbox',
        'meta_value' => 'yes'
    );
    $featured = new WP_Query($args);
 
if ($featured->have_posts()): while($featured->have_posts()): $featured->the_post(); ?>
<h3><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h3>
<p class="details">By <a href="<?php the_author_posts() ?>"><?php the_author(); ?> </a> / On <?php echo get_the_date('F j, Y'); ?> / In <?php the_category(', '); ?></p>
<?php if (has_post_thumbnail()) : ?>
 
<figure> <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> </figure>
<p ><?php the_excerpt();?></p>
<?php
endif;
endwhile; else:
endif;
?>
  </div>
     

    

    <div>

             <h2 class="text-center"> Latest Posts </h2>

            <?php
            $the_query = new WP_Query( 'posts_per_page=5' );
            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

                get_template_part( 'content/content-sidebar', get_post_format() );

            endwhile; endif;
            ?>
    


    </div>

   <div class="banner_add">
    <img src="<?php echo get_template_directory_uri();?>/image/banner_add.jpg" alt="banner_add" width="100%" height=""/>

</div>



    <?php dynamic_sidebar('ps_right_sidebar3');?>




   <div>

       <h3>Most Viewed</h3>
<ul>
    <?php $popular = new WP_Query(array('posts_per_page'=>5, 'meta_key'=>'popular_posts', 'orderby'=>'comment_count', 'order'=>'DESC'));
    while ($popular->have_posts()) : $popular->the_post(); ?>
    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
    <?php endwhile; wp_reset_postdata(); ?>
</ul>
<!--  https://digwp.com/2016/03/diy-popular-posts/ -->
   </div>

 
<?php get_search_form(); ?>


  <div>
    <h1 style="color:red"> latest post: </h1>

        <?php content_sidebar(); ?>
  </div>

</div><!-- /.blog-sidebar -->