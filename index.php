<?php get_header(); ?>

<div class="row">

    <div class="col-md-8 blog-main">


        <?php
        if ( have_posts() ) : while ( have_posts() ) : the_post();

            if ( has_post_format( 'image' )) {
                get_template_part( 'content/content-image', get_post_format() );
            }

            elseif ( has_post_format( 'audio' )) {
                get_template_part( 'content/content-audio', get_post_format() );
            }

            elseif ( has_post_format( 'video' )) {
                get_template_part( 'content/content-video', get_post_format() );
            }

            elseif ( has_post_format( 'quote' )) {
                get_template_part( 'content/content-quote', get_post_format() );
            }

            elseif ( has_post_format( 'gallery' )) {
                get_template_part( 'content/content-gallery', get_post_format() );
            }

             elseif ( has_post_format( 'aside' )) {
                get_template_part( 'content/content-aside', get_post_format() );
            }

             elseif ( has_post_format( 'chat' )) {
                get_template_part( 'content/content-chat', get_post_format() );
            }

             elseif ( has_post_format( 'link' )) {
                get_template_part( 'content/content-link', get_post_format() );
            }

             elseif ( has_post_format( 'status' )) {
                get_template_part( 'content/content-status', get_post_format() );
            }

             elseif ( has_post_format( 'Standard' )) {
                get_template_part( 'content/content-standard', get_post_format() );
            }

            

            else {
                get_template_part('content/content-default', get_post_format());
            }

        endwhile;
        ?>

            <?php ps_pagination(); ?>


            <?php

      else :
            get_template_part( 'content/content', 'none' );
        endif;
        ?>

    </div> <!-- /.blog-main -->

    <?php get_sidebar(); ?>

</div> <!-- /.row -->

<?php get_footer(); ?>

