<div class="blog-post">
    <div class="image-content">
  
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('medium_large');
        }
        ?>

   

    

        <p class="image-post text-center">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <?php the_time( get_option( 'date_format' ) ); ?>/ <?php the_category(', '); ?>  <?php comments_number(); ?> </p>

    

    
        <h2 class="image-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

    

   <div class="image-content">
        <?php the_content(); ?>
   </div>
    
     <div class="text-center">
    <a class="text-center read_more"href="<?php the_permalink(); ?>"> Read More </a> 
     </div>
  <!--  <?php  the_post_thumbnail('medium'); ?> -->

    </div>
</div><!-- /.blog-post -->
