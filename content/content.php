<div class="blog-post">
    <h2 class="blog-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p class="blog-post-meta"><?php the_time( get_option( 'date_format' ) ); ?> by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a></p>

<?php
    if ( has_post_thumbnail() ) {
                the_post_thumbnail('medium_large');
            }
            
 the_content(); ?>

  <!--   <a href="<?php comments_link(); ?>">
        <?php
        printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( 						get_comments_number() ) ); ?>
    </a> -->

</div><!-- /.blog-post -->