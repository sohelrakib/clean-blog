<div>
    <h2 class="blog-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

    <p class="blog-post-meta">BY <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> ON <?php the_time( get_option( 'date_format' ) ); ?>/ <?php the_category(', '); ?>  <?php comments_number(); ?> </p>


  

  



</div><!-- /.blog-post -->