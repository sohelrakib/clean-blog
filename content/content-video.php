<div class="blog-post">

    <div class="video-content">

        <?php the_content(); ?>

        <p class="video-post text-center">by <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">  <?php the_author(); ?>  </a> on <span class="time-catagory"> <?php the_time( get_option( 'date_format' ) ); ?> /</span> <?php the_category(', '); ?>  <?php comments_number(); ?> </p>

    

    
        <h2 class="video-title text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

  

    </div>

</div><!-- /.blog-post -->
